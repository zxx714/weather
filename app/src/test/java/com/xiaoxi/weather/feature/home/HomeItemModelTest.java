package com.xiaoxi.weather.feature.home;

import android.support.annotation.NonNull;

import com.xiaoxi.weather.api.model.CurrentWeather;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HomeItemModelTest {
    @Mock
    private CurrentWeather currentWeather;

    @Test
    public void getWindDirection() throws Exception {
        testWindDegree(0, "N");
        testWindDegree(22, "N");
        testWindDegree(23, "NE");
        testWindDegree(67, "NE");
        testWindDegree(68, "E");
        testWindDegree(337, "NW");
        testWindDegree(338, "N");
    }

    private void testWindDegree(int degree, @NonNull String expectResult) {
        when(currentWeather.getWindDegree()).thenReturn(degree);
        Assert.assertEquals(expectResult, new HomeItemModel(currentWeather).getWindDirection());
    }

}