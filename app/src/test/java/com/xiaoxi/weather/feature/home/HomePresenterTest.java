package com.xiaoxi.weather.feature.home;

import com.xiaoxi.weather.RxTestSchedulerRule;
import com.xiaoxi.weather.api.model.CurrentWeather;
import com.xiaoxi.weather.api.model.ForecastResponse;
import com.xiaoxi.weather.broker.WeatherBroker;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import io.reactivex.Single;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HomePresenterTest {
    @ClassRule
    public static final RxTestSchedulerRule schedulers = RxTestSchedulerRule.instance;

    @Mock
    private WeatherBroker weatherBroker;
    @Mock
    private HomeActivity activity;
    @Mock
    private CurrentWeather currentWeather;
    @Mock
    private ForecastResponse forecastResponse;
    @Captor
    private ArgumentCaptor<HomeModel> homeModelArgumentCaptor;

    private HomePresenter presenter;

    @Before
    public void setup() {
        presenter = new HomePresenter(weatherBroker, activity);
        when(weatherBroker.search(Mockito.anyString())).thenReturn(Single.just(currentWeather));
        when(currentWeather.getCityId()).thenReturn(123);
        when(currentWeather.getCityName()).thenReturn("Wellington");
        when(weatherBroker.forecast(Mockito.anyInt())).thenReturn(Single.just(forecastResponse));
    }

    @Test
    public void happyFlow() {
        // run
        presenter.search("w");
        presenter.search("we");
        presenter.search("wel");
        schedulers.testScheduler.advanceTimeBy(600, TimeUnit.MILLISECONDS);
        // verify
        verify(activity).showLoading();
        verify(weatherBroker, never()).search("w");
        verify(weatherBroker, never()).search("we");
        verify(weatherBroker).search("wel");
        verify(activity).populateData(homeModelArgumentCaptor.capture());
        Assert.assertEquals("Wellington", homeModelArgumentCaptor.getValue().getCityName());
    }

    @Test
    public void networkError() {
        final Single<CurrentWeather> error = Single.error(new UnknownHostException());
        when(weatherBroker.search(Mockito.anyString())).thenReturn(error);
        // run
        presenter.search("wel");
        schedulers.testScheduler.advanceTimeBy(600, TimeUnit.MILLISECONDS);
        verify(activity).showLoading();
        verify(activity, never()).populateData(any(HomeModel.class));
        verify(activity).showError(anyString());
    }
}