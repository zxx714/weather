package com.xiaoxi.weather;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.util.concurrent.Callable;

import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.TestScheduler;

public class RxTestSchedulerRule implements TestRule {
    public static final RxTestSchedulerRule instance = new RxTestSchedulerRule();

    public final TestScheduler testScheduler = new TestScheduler();

    private RxTestSchedulerRule() {

    }

    @Override
    public Statement apply(final Statement base, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                RxJavaPlugins.reset();

                RxJavaPlugins.setInitIoSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
                    @Override
                    public Scheduler apply(@NonNull final Callable<Scheduler> schedulerCallable) throws Exception {
                        return testScheduler;
                    }
                });
                RxJavaPlugins.setInitComputationSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
                    @Override
                    public Scheduler apply(@NonNull final Callable<Scheduler> schedulerCallable) throws Exception {
                        return testScheduler;
                    }
                });
                RxJavaPlugins.setInitNewThreadSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
                    @Override
                    public Scheduler apply(@NonNull final Callable<Scheduler> schedulerCallable) throws Exception {
                        return testScheduler;
                    }
                });
                RxJavaPlugins.setInitSingleSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
                    @Override
                    public Scheduler apply(@NonNull final Callable<Scheduler> schedulerCallable) throws Exception {
                        return testScheduler;
                    }
                });
                RxAndroidPlugins.setMainThreadSchedulerHandler(new Function<Scheduler, Scheduler>() {
                    @Override
                    public Scheduler apply(@NonNull final Scheduler scheduler) throws Exception {
                        return testScheduler;
                    }
                });
                RxAndroidPlugins.initMainThreadScheduler(new Callable<Scheduler>() {
                    @Override
                    public Scheduler call() throws Exception {
                        return testScheduler;
                    }
                });
                RxAndroidPlugins.setInitMainThreadSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
                    @Override
                    public Scheduler apply(@NonNull final Callable<Scheduler> schedulerCallable) throws Exception {
                        return testScheduler;
                    }
                });
                RxAndroidPlugins.setInitMainThreadSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
                    @Override
                    public Scheduler apply(@NonNull final Callable<Scheduler> schedulerCallable) throws Exception {
                        return testScheduler;
                    }
                });

                try {
                    base.evaluate();
                }
                finally {
                    RxJavaPlugins.reset();
                    RxAndroidPlugins.reset();
                }
            }
        };
    }
}
