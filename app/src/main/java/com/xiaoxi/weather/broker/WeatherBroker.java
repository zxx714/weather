package com.xiaoxi.weather.broker;

import com.xiaoxi.weather.App;
import com.xiaoxi.weather.api.model.CurrentWeather;
import com.xiaoxi.weather.api.model.ForecastResponse;
import com.xiaoxi.weather.api.service.WeatherService;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class WeatherBroker {
    private final WeatherService service;
    private final Scheduler queryScheduler = Schedulers.io();

    public WeatherBroker() {
        this.service = new Retrofit.Builder()
                .client(App.getInstance().getOkHttpClient().build())
                .baseUrl("http://api.openweathermap.org/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(WeatherService.class);
    }

    public Single<CurrentWeather> search(String query) {
        return service.getCurrentWeather(query).subscribeOn(queryScheduler);
    }

    public Single<ForecastResponse> forecast(int cityId) {
        return service.getForecastWeather(cityId).subscribeOn(queryScheduler);
    }
}
