package com.xiaoxi.weather;

import android.annotation.SuppressLint;
import android.app.Application;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.xiaoxi.weather.broker.WeatherBroker;

import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

public class App extends Application {
    private static App instance;

    private WeatherBroker weatherBroker;

    public static App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Logger.addLogAdapter(new AndroidLogAdapter());
    }

    public synchronized WeatherBroker getWeatherBroker() {
        if (weatherBroker == null) {
            weatherBroker = new WeatherBroker();
        }
        return weatherBroker;
    }

    public synchronized OkHttpClient.Builder getOkHttpClient() {
        final OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            builder.addNetworkInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
            builder.hostnameVerifier(new HostnameVerifier() {
                @SuppressLint("BadHostnameVerifier")
                @Override
                public boolean verify(String s, SSLSession sslSession) {
                    return true;
                }
            });
            try {
                final TrustManager[] trustAllCerts = new TrustManager[]{
                        new X509TrustManager() {
                            @SuppressLint("TrustAllX509TrustManager")
                            @Override
                            public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                            }

                            @SuppressLint("TrustAllX509TrustManager")
                            @Override
                            public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                            }

                            @Override
                            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                return new java.security.cert.X509Certificate[]{};
                            }
                        }
                };

                // Install the all-trusting trust manager
                final SSLContext sslContext = SSLContext.getInstance("SSL");
                sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
                // Create an ssl socket factory with our all-trusting manager
                //noinspection deprecation
                builder.sslSocketFactory(sslContext.getSocketFactory());
            } catch (Exception e) {
                Logger.e(App.class.getSimpleName(), e.getMessage());
            }
        }
//        builder.addInterceptor(chain -> {
//            if (!NetworkUtil.hasNetworkConnection(App.getInstance())) {
//                try {
//                    Thread.sleep(Constants.LOADING_ANIMATION_TIME);
//                }
//                catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                throw new VectorException(ErrorType.NoConnection);
//            }
//            try {
//                return chain.proceed(chain.request());
//            }
//            catch (SocketTimeoutException e) {
//                throw new VectorException(ErrorType.Timeout);
//            }
//        });
//        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
//            ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
//                    .tlsVersions(TlsVersion.TLS_1_2)
//                    .cipherSuites(CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
//                            CipherSuite.TLS_DHE_RSA_WITH_AES_256_GCM_SHA384)
//                    .build();
//            builder.connectionSpecs(Collections.singletonList(spec));
//        }
        return builder;
    }
}
