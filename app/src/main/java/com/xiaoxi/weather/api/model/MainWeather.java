package com.xiaoxi.weather.api.model;

import com.google.gson.annotations.SerializedName;

public class MainWeather {
    int humidity;
    @SerializedName("temp_min")
    private float tempMin;
    @SerializedName("temp_max")
    private float tempMax;

    public int getHumidity() {
        return humidity;
    }

    float getTempMin() {
        return tempMin;
    }

    float getTempMax() {
        return tempMax;
    }
}
