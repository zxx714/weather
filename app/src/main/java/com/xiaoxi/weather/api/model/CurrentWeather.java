package com.xiaoxi.weather.api.model;

import android.annotation.SuppressLint;
import android.support.annotation.DrawableRes;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@SuppressWarnings("unused")
public class CurrentWeather {
    @SuppressLint("SimpleDateFormat")
    private static SimpleDateFormat format = new SimpleDateFormat("EEEE");

    @SerializedName("name")
    private String cityName;
    @SerializedName("id")
    private int cityId;
    @SerializedName("main")
    private MainWeather mainWeather;
    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    @SerializedName("weather")
    private List<WeatherCondition> conditions;
    private Wind wind;
    private long dt;

    public String getCityName() {
        return cityName;
    }

    public float getMaxTemp() {
        return mainWeather.getTempMax();
    }

    public float getMinTemp() {
        return mainWeather.getTempMin();
    }

    public String getDate() {
        return format.format(new Date(dt * 1000));
    }

    public String getWeatherCondition() {
        return conditions.get(0).description;
    }

    public int getCityId() {
        return cityId;
    }

    public int getWindDegree() {
        return wind.deg;
    }

    public int getHumidity() {
        return mainWeather.humidity;
    }

    @DrawableRes
    public int getIcon() {
        return conditions.get(0).getIcon();
    }
}
