package com.xiaoxi.weather.api.service;

import com.xiaoxi.weather.api.model.CurrentWeather;
import com.xiaoxi.weather.api.model.ForecastResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherService {
    @GET("data/2.5/weather?appid=ef574888b32847985ddc2dea90acc9a6")
    Single<CurrentWeather> getCurrentWeather(@Query("q") String city);

    @GET("data/2.5/forecast/daily?appid=ef574888b32847985ddc2dea90acc9a6")
    Single<ForecastResponse> getForecastWeather(@Query("id") int cityId);
}
