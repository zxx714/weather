package com.xiaoxi.weather.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ForecastResponse {
    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    @SerializedName("list")
    private List<ForecastWeather> weathers;

    public List<ForecastWeather> getWeathers() {
        int size = Math.min(5, weathers.size());
        return weathers.subList(0, size);
    }
}
