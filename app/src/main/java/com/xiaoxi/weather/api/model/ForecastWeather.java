package com.xiaoxi.weather.api.model;

import android.annotation.SuppressLint;
import android.support.annotation.DrawableRes;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@SuppressWarnings("unused")
public class ForecastWeather {
    @SuppressLint("SimpleDateFormat")
    private static SimpleDateFormat format = new SimpleDateFormat("EEEE");

    private long dt;
    @SerializedName("deg")
    private int windDegree;
    private int humidity;
    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    private List<WeatherCondition> weather;
    private Temperature temp;

    public String getDate() {
        return format.format(new Date(dt * 1000));
    }

    public float getMaxTemp() {
        return temp.max;
    }

    public float getMinTemp() {
        return temp.min;
    }

    public String getWeatherCondition() {
        return weather.get(0).description;
    }

    public int getWindDegree() {
        return windDegree;
    }

    public int getHumidity() {
        return humidity;
    }

    @DrawableRes
    public int getIcon() {
        return weather.get(0).getIcon();
    }
}
