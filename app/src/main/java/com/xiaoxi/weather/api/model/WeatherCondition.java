package com.xiaoxi.weather.api.model;

import android.support.annotation.DrawableRes;

import com.orhanobut.logger.Logger;
import com.xiaoxi.weather.R;

class WeatherCondition {
    private int id;
    String description;

    @DrawableRes
    public int getIcon() {
        if (id == 761 || id == 781 || (id >= 200 && id <= 232)) {
            return R.drawable.ic_storm;
        } else if (id >= 300 && id <= 321) {
            return R.drawable.ic_lightrain;
        } else if ((id >= 500 && id <= 504) || (id >= 520 && id <= 531)) {
            return R.drawable.ic_rain;
        } else if (id == 511 || (id >= 600 && id <= 622)) {
            return R.drawable.ic_snow;
        } else if (id >= 701 && id <= 761) {
            return R.drawable.ic_fog;
        } else if (id == 800) {
            return R.drawable.ic_clear;
        } else if (id >= 801 && id <= 804) {
            return R.drawable.ic_cloud;
        }
        Logger.e("Unhandled case, id=" + id);
        return R.drawable.ic_clear;
    }
}
