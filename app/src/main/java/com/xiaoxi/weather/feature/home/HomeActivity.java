package com.xiaoxi.weather.feature.home;

import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.xiaoxi.weather.App;
import com.xiaoxi.weather.R;
import com.xiaoxi.weather.feature.common.BaseActivity;
import com.xiaoxi.weather.feature.detail.DetailActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class HomeActivity extends BaseActivity<HomePresenter> {
    private final static String STATE_MODEL = "STATE_MODEL";

    @BindView(R.id.home_city_name)
    TextView cityName;
    @BindView(R.id.home_recycler)
    RecyclerView recyclerView;
    @BindView(R.id.home_content)
    LinearLayout content;
    @BindView(R.id.home_loading)
    ProgressBar loadingBar;
    @BindView(R.id.home_error)
    TextView errorView;
    private Adapter adapter;
    private HomeModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        presenter = new HomePresenter(App.getInstance().getWeatherBroker(), this);
        adapter = new Adapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if (model != null) {
            populateData(model);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(STATE_MODEL, model);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        model = (HomeModel) savedInstanceState.getSerializable(STATE_MODEL);
    }

    @OnTextChanged(R.id.home_search)
    void onSearch(CharSequence query) {
        presenter.search(query.toString());
    }

    void showLoading() {
        loadingBar.setVisibility(View.VISIBLE);
        content.setVisibility(View.GONE);
        errorView.setVisibility(View.GONE);
    }

    void populateData(HomeModel homeModel) {
        model = homeModel;
        cityName.setText(homeModel.getCityName());
        adapter.setItems(homeModel.items);
        content.setVisibility(View.VISIBLE);
        hideLoading();
    }

    void showError(String errorMessage) {
        hideLoading();
        content.setVisibility(View.GONE);
        errorView.setText(errorMessage);
        errorView.setVisibility(View.VISIBLE);
    }

    private void hideLoading() {
        loadingBar.setVisibility(View.GONE);
    }

    private class Adapter extends RecyclerView.Adapter<ItemViewHolder> {
        private List<HomeItemModel> items = new ArrayList<>();

        @Override
        public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            return new ItemViewHolder(inflater.inflate(R.layout.item_weather, parent, false));
        }

        @Override
        public void onBindViewHolder(ItemViewHolder holder, int position) {
            holder.bind(items.get(position));
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        void setItems(List<HomeItemModel> items) {
            this.items = items;
            notifyDataSetChanged();
        }
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.weather_date)
        TextView date;
        @BindView(R.id.weather_temp_row)
        LinearLayout tempRow;
        @BindView(R.id.weather_max_temp)
        TextView maxTemp;
        @BindView(R.id.weather_min_temp)
        TextView minTemp;
        @BindView(R.id.weather_condition)
        TextView condition;
        @BindView(R.id.weather_condition_icon)
        ImageView icon;
        private HomeItemModel model;

        ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.weather_root)
        void onClick() {
            Pair<View, String> trans1 = new Pair<>((View) date, "date");
            Pair<View, String> trans2 = new Pair<>((View) tempRow, "tempRow");
            Pair<View, String> trans3 = new Pair<>((View) condition, "condition");
            Pair<View, String> trans4 = new Pair<>((View) icon, "conditionIcon");
            final Bundle bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(HomeActivity.this,
                    trans1, trans2, trans3, trans4).toBundle();
            DetailActivity.start(itemView.getContext(), model, cityName.getText().toString(), bundle);
        }

        void bind(HomeItemModel model) {
            this.model = model;
            date.setText(model.getDate());
            maxTemp.setText(model.getMaxTemp());
            minTemp.setText(model.getMinTemp());
            condition.setText(model.getWeatherCondition());
            icon.setImageResource(model.getIcon());
        }
    }
}
