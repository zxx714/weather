package com.xiaoxi.weather.feature.home;

import com.orhanobut.logger.Logger;
import com.xiaoxi.weather.api.model.CurrentWeather;
import com.xiaoxi.weather.api.model.ForecastResponse;
import com.xiaoxi.weather.broker.WeatherBroker;
import com.xiaoxi.weather.feature.common.BasePresenter;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import retrofit2.HttpException;

class HomePresenter extends BasePresenter<HomeActivity> {
    private WeatherBroker broker;
    private ObservableEmitter<String> searchEmitter;

    HomePresenter(WeatherBroker broker, HomeActivity activity) {
        this.broker = broker;
        this.activity = activity;
        Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(ObservableEmitter<String> e) throws Exception {
                searchEmitter = e;
            }
        }).debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String query) throws Exception {
                        doSearch(query);
                    }
                });
    }

    void search(String query) {
        searchEmitter.onNext(query);
    }

    private void doSearch(final String query) {
        activity.showLoading();
        compositeDisposable.add(broker.search(query).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<CurrentWeather>() {
            @Override
            public void accept(final CurrentWeather currentWeather) throws Exception {
                Logger.i("Succ");
                compositeDisposable.add(broker.forecast(currentWeather.getCityId())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<ForecastResponse>() {
                            @Override
                            public void accept(ForecastResponse forecastResponse) throws Exception {
                                activity.populateData(new HomeModel(currentWeather, forecastResponse));
                                compositeDisposable.clear();
                            }
                        }));
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                if (throwable instanceof IOException) {
                    activity.showError("Please check your network connection");
                } else if (throwable instanceof HttpException) {
                    HttpException exception = (HttpException) throwable;
                    if (exception.code() == 404) {
                        activity.showError("Can't find city '" + query + "'");
                    }
                } else {
                    activity.showError("Something wrong, please try again later");
                }

            }
        }));
    }
}
