package com.xiaoxi.weather.feature.common;

import io.reactivex.disposables.CompositeDisposable;

public class BasePresenter<Activity extends BaseActivity> {
    protected Activity activity;
    protected CompositeDisposable compositeDisposable = new CompositeDisposable();

    final void onDetached() {
        compositeDisposable.dispose();
        activity = null;
    }

    final void attachView(final Activity activity) {
        this.activity = activity;
    }
}
