package com.xiaoxi.weather.feature.detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xiaoxi.weather.R;
import com.xiaoxi.weather.feature.common.BaseActivity;
import com.xiaoxi.weather.feature.home.HomeItemModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends BaseActivity {
    private final static String ARG_MODEL = "ARG_MODEL";
    private final static String ARG_CITY_NAME = "ARG_CITY_NAME";

    @BindView(R.id.detail_date)
    TextView date;
    @BindView(R.id.detail_temp_row)
    LinearLayout tempRow;
    @BindView(R.id.detail_max_temp)
    TextView maxTemp;
    @BindView(R.id.detail_min_temp)
    TextView minTemp;
    @BindView(R.id.detail_condition)
    TextView condition;
    @BindView(R.id.detail_condition_icon)
    ImageView conditionIcon;
    @BindView(R.id.detail_wind)
    TextView windDirection;
    @BindView(R.id.detail_humidity)
    TextView humidity;

    public static void start(Context context, HomeItemModel model, String cityName, Bundle bundle) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(ARG_MODEL, model);
        intent.putExtra(ARG_CITY_NAME, cityName);
        context.startActivity(intent, bundle);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        HomeItemModel model = (HomeItemModel) getIntent().getSerializableExtra(ARG_MODEL);
        date.setText(model.getDate());
        ViewCompat.setTransitionName(date, "date");
        ViewCompat.setTransitionName(tempRow, "tempRow");
        ViewCompat.setTransitionName(condition, "condition");
        ViewCompat.setTransitionName(conditionIcon, "conditionIcon");
        maxTemp.setText(model.getMaxTemp());
        minTemp.setText(model.getMinTemp());
        conditionIcon.setImageResource(model.getIcon());
        condition.setText(model.getWeatherCondition());
        windDirection.setText(model.getWindDirection());
        humidity.setText(model.getHumidity());
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getIntent().getStringExtra(ARG_CITY_NAME));
        }
    }
}
