package com.xiaoxi.weather.feature.home;

import com.xiaoxi.weather.api.model.CurrentWeather;
import com.xiaoxi.weather.api.model.ForecastResponse;
import com.xiaoxi.weather.api.model.ForecastWeather;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class HomeModel implements Serializable {
    final private String cityName;
    final List<HomeItemModel> items;

    HomeModel(final CurrentWeather currentWeather, final ForecastResponse forecastResponse) {
        cityName = currentWeather.getCityName();
        items = new ArrayList<>();
        items.add(new HomeItemModel(currentWeather));
        for (ForecastWeather weather : forecastResponse.getWeathers()) {
            items.add(new HomeItemModel(weather));
        }
    }

    String getCityName() {
        return cityName;
    }

    @Override
    public String toString() {
        return "HomeModel{" +
                "cityName='" + cityName + '\'' +
                ", items=" + items +
                '}';
    }
}
