package com.xiaoxi.weather.feature.home;

import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;

import com.xiaoxi.weather.api.model.CurrentWeather;
import com.xiaoxi.weather.api.model.ForecastWeather;

import java.io.Serializable;
import java.util.Locale;

public class HomeItemModel implements Serializable {
    final private String date;
    final private float maxTemp;
    final private float minTemp;
    final private String weatherCondition;
    final private int windDegree;
    final private int humidity;
    @DrawableRes
    final private int icon;

    HomeItemModel(final @NonNull CurrentWeather currentWeather) {
        date = currentWeather.getDate();
        maxTemp = currentWeather.getMaxTemp();
        minTemp = currentWeather.getMinTemp();
        weatherCondition = currentWeather.getWeatherCondition();
        windDegree = currentWeather.getWindDegree();
        humidity = currentWeather.getHumidity();
        icon = currentWeather.getIcon();
    }

    HomeItemModel(final @NonNull ForecastWeather forecastWeather) {
        date = forecastWeather.getDate();
        maxTemp = forecastWeather.getMaxTemp();
        minTemp = forecastWeather.getMinTemp();
        weatherCondition = forecastWeather.getWeatherCondition();
        windDegree = forecastWeather.getWindDegree();
        humidity = forecastWeather.getHumidity();
        icon = forecastWeather.getIcon();
    }

    @Override
    public String toString() {
        return "HomeItemModel{" +
                "date='" + date + '\'' +
                ", maxTemp=" + maxTemp +
                ", minTemp=" + minTemp +
                ", weatherCondition='" + weatherCondition + '\'' +
                ", windDegree=" + windDegree +
                ", humidity=" + humidity +
                '}';
    }

    public String getMaxTemp() {
        return convertToCentigrade(maxTemp);
    }

    public String getMinTemp() {
        return convertToCentigrade(minTemp);
    }

    public String getDate() {
        return date;
    }

    public String getWeatherCondition() {
        return weatherCondition;
    }

    @DrawableRes
    public int getIcon() {
        return icon;
    }
    public String getWindDirection() {
        if (windDegree < 22.5) {
            return "N";
        } else if (windDegree < 67.5) {
            return "NE";
        } else if (windDegree < 112.5) {
            return "E";
        } else if (windDegree < 157.5) {
            return "SE";
        } else if (windDegree < 202.5) {
            return "S";
        } else if (windDegree < 247.5) {
            return "SW";
        } else if (windDegree < 292.5) {
            return "W";
        } else if (windDegree < 337.5) {
            return "NW";
        } else {
            return "N";
        }
    }

    public String getHumidity() {
        return humidity + "%";
    }

    private static String convertToCentigrade(float k) {
        return String.format(Locale.ENGLISH, "%d°C", Math.round(k - 273.15f));
    }
}
